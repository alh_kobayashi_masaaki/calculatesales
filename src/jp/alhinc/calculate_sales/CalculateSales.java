package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String TOTAL_AMOUNT_GREATER_THAN_10_DIGITS = "合計金額が10桁を超えました";
	private static final String NVALID_BRANCH_CODE = "の支店コードが不正です";
	private static final String NVALID_COMMODITY_CODE = "の商品コードが不正です";
	private static final String NVALID_FORMAT = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		//3_3-1
		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, "支店", "[0-9]{3}", branchNames, branchSales)) {
			return;
		}

		//商品定義ファイル読み込み 1-4
		if(!readFile(args[0], FILE_NAME_COMMODITY_LST, "商品", "[a-zA-Z0-9]{8}", commodityNames, commoditySales)){
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2) 売上ファイル取得
		File[] files = new File(args[0]).listFiles();
		ArrayList<String> salesFiles = new ArrayList<String>();

		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches("[0-9]{8}.rcd")) {
				salesFiles.add(files[i].getName());
			}
		}

		//3_2-1
		Collections.sort(salesFiles);
		for(int i = 0; i < salesFiles.size() - 1; i++) {
			int former = Integer.parseInt(salesFiles.get(i).substring(0,8));
			int latter = Integer.parseInt(salesFiles.get(i + 1).substring(0,8));

			if((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//branchSalesに売上金額格納
		BufferedReader br = null;

		for(int i = 0; i < salesFiles.size(); i++) {
			try {
				File file = new File(args[0], salesFiles.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line;
				ArrayList<String>  lines = new ArrayList<String>(); //0:支店コード 1:商品コード 2:売上金額
				while((line = br.readLine()) != null) {
					lines.add(line);
				}

				//3_2-5
				if(lines.size() != 3) {
					System.out.println(salesFiles.get(i) + NVALID_FORMAT);
					return;
				}
				//3_2-3
				if (!branchNames.containsKey(lines.get(0))) {
					System.out.println(salesFiles.get(i) + NVALID_BRANCH_CODE);
					return;
				}
				//3_2-4
				if (!commodityNames.containsKey(lines.get(1))) {
					System.out.println(salesFiles.get(i) + NVALID_COMMODITY_CODE);
					return;
				}

				//3_3-2
				if(!lines.get(2).matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				//3_2-2
				long fileSale = Long.parseLong(lines.get(2));
				Long saleAmount = branchSales.get(lines.get(0)) + fileSale;
				Long commodityAmount = commoditySales.get(lines.get(1)) + fileSale;
				if(saleAmount >= 10000000000L || commodityAmount >= 10000000000L){
					System.out.println(TOTAL_AMOUNT_GREATER_THAN_10_DIGITS);
					return;
				}

				branchSales.put(lines.get(0), saleAmount);
				commoditySales.put(lines.get(1), commodityAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if(br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
					}
				}
			}
		}

		// 支店別集計ファイル書き込み
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		//商品別集計ファイル書き込み
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 定義名
	 * @param matchsの正規表現
	 * @param コードと名前を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String definitionName, String regex,
			                          Map<String, String> names, Map<String, Long> sales) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if(!file.exists()){
				System.out.println(definitionName + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");//支店定義ファイル 0:支店コード 1:支店名  商品定義ファイル 0:商品コード 1:商品名

				//3_1-2
				if((items.length != 2) || (!items[0].matches(regex))){
					System.out.println(definitionName + FILE_INVALID_FORMAT);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}


	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param コードと名前を保持するMap
	 * @param コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName,
			                           Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for(String key : names.keySet()) {
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
